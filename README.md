# ModelCompressionCourse

## Experiments with instance segmentation task

## Environment 

Download COCO val dataset:

https://cocodataset.org/#download

http://images.cocodataset.org/zips/val2017.zip (2017 Val images [5K/1GB])

http://images.cocodataset.org/annotations/annotations_trainval2017.zip (2017 Train/Val annotations [241MB])

Mount dataset in docker-compose.yml

Run docker:
- docker-compose -f ./docker-compose.yml build
- docker-compose -f ./docker-compose.yml run --rm shell

Run jupyter inside docker:
- jupyter notebook --ip 0.0.0.0 --no-browser --allow-root

## Basic performance

gpu: NVIDIA GeForce GTX 1050 3GB

cpu: Intel® Core™ i5-9300H CPU @ 2.40GHz × 8

|                                      | mAP (segm) | mAP (segm, IOU = 0.75) | mAP (segm, person) | mAP (segm, person, IOU = 0.75) | mean time, s | model size, mb |
| ------------------------------------ | ---------- | ---------------------- | ------------------ | ------------------------------ | ------------ | -------------- |
| basic model, gpu                     | 0.397      | 0.423                  | 0.447              | 0.471                          | 0.423        | 169.78         |
| basic model, cpu                     | 0.397      | 0.423                  | 0.447              | 0.471                          | 3.183        | 169.78         |
| dynamic quantize, linear layers, cpu | 0.396      | 0.418                  | 0.446              | 0.469                          | 3.112        | 114.99         |
| pruning, gpu                         | 0.306      | 0.321                  | 0.384              | 0.366                          | 0.424        | 169.78         |

