lightning
pandas
Pillow
isort
black
flake8
pylint
opencv-python
notebook==6.4.10
traitlets==5.9.0
pycocotools
ipywidgets