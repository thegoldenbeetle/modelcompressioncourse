import torch
from torchvision import models

def get_model_instance_segmentation():
    return models.detection.maskrcnn_resnet50_fpn(weights=models.detection.MaskRCNN_ResNet50_FPN_Weights.DEFAULT)

