import numpy as np
import torch
import matplotlib.pyplot as plt
import torchvision.transforms.functional as F

from matplotlib.font_manager import FontProperties
from torchvision.io import read_image
from torchvision.utils import draw_segmentation_masks, draw_bounding_boxes


def get_float_image(img_path):
    '''
    По переданному пути к файлу с изображением возвращает тензор [C, H, W] \
    со значениями в интервале [0..1].
    '''
    img = read_image(str(img_path))
    return img / 255
    

def show_img(imgs):
    '''
    Функция для отображения изображений в ноутбуке.
    
        Параметры:
            imgs: изображение или список изображений,
                  представляющих собой тензоры [C, H, W].
    '''
    if not isinstance(imgs, list):
        imgs = [imgs]
    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    return fig


def show_coco_dataset_item(coco_dataset_item, alpha=0.5, width=1):
    '''
    Функция для отображения элемента из датасета в формате coco.
    Отображает исходное изображение с боксами и наложенными масками объектов.
    '''
    image = coco_dataset_item[0]
    bool_masks = coco_dataset_item[1]['masks']
    boxes = coco_dataset_item[1]['boxes']
    image *= 255
    image = image.type(torch.ByteTensor)
    bool_masks = bool_masks.type(torch.BoolTensor)
    show_img(draw_bounding_boxes(draw_segmentation_masks(image, bool_masks, alpha=alpha),\
                                 boxes=boxes, width=width))
                                 

def show_instance_segmentation_by_model_predictions(img, predictions, score_thres=0.5, \
                                             mask_thres=0.5, alpha=0.7, width=1):
    '''
    Отображает предсказанные маску и боксы с порогами score_thres, mask_thres для объектов 
    определенных классов classes, по-умолчанию для person.
    img - тензор [C, H, W] со значениями [0..1].
    '''
    pred_scores = predictions['scores']
    pred_masks = predictions['masks']
    pred_labels = predictions['labels']
    pred_boxes = predictions['boxes']

    img = img * 255
    img = img.type(torch.ByteTensor)

    if len(pred_boxes) == 0 or len(pred_masks) == 0:
        show_img(img)
        return

    high_scores_bool = pred_scores > score_thres
    
    indexes = np.argwhere(high_scores_bool).squeeze(1)
    if len(indexes) == 0:
        show_img(img)
        return
    
    masks = pred_masks[indexes]
    bool_masks = masks > mask_thres
    bool_masks = bool_masks.squeeze(1)
    bool_masks = torch.from_numpy(bool_masks)
    
    boxes = torch.from_numpy(pred_boxes[indexes])
        
    show_img(draw_bounding_boxes(draw_segmentation_masks(img, bool_masks, alpha=alpha), \
                    boxes=boxes, width=width))
