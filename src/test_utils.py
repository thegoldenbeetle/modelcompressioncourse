import sys
import torch
import time
from src.references.detection import utils
from src.references.detection.engine import evaluate


@torch.no_grad()
def get_inference_time(img, model, device):
    '''
    Измерение времени инференса для прохода по одному изображению.
    '''    
    start = time.time()
    pred = model(img.unsqueeze(0))
    if torch.cuda.is_available() and device == torch.device('cuda'):
        torch.cuda.synchronize()
    end = time.time()
    
    return end - start


def get_model_size_mb(model):
    param_size = 0
    for param in model.parameters():
        param_size += param.nelement() * param.element_size()
    buffer_size = 0
    for buffer in model.buffers():
        buffer_size += buffer.nelement() * buffer.element_size()

    size_all_mb = (param_size + buffer_size) / 1024 ** 2    
    return size_all_mb


@torch.no_grad()
def get_model_prediction_one_image(img, model, device):
    '''
    Предсказание модели для одного входного изображения.

        Параметры:
            img: изображение должно быть тензором [C, H, W] со значениями в интервале [0..1]
            model: модель, осуществляющая предсказание. Предсказание должно
                   быть словарем с ключами scores, masks, labels, boxes
            device: устройство для вычислений, torch.device

        Возвращаемое значение:
            словарь, содержащий scores, masks, labels, boxes
    '''
    img = img.to(device)
    model = model.to(device)

    model = model.eval()

    pred = model(img.unsqueeze(0))    
    
    return {
        k: v.detach().cpu().numpy()
        for k, v in pred[0].items()
        if k in ['scores', 'masks', 'labels', 'boxes']
    }


@torch.no_grad()
def test_model_on_dataset(model, dataset, device, cat_ids=None, batch_size=1):
    '''
    Тестирование модели на переданном датасете.
    '''
    model = model.to(device)    
    data_loader = torch.utils.data.DataLoader(dataset, batch_size, shuffle=False, \
                                              collate_fn=utils.collate_fn)

    return evaluate(model, data_loader, device=device, cat_ids=cat_ids)


