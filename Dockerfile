FROM nvidia/cuda:11.7.1-devel-ubuntu20.04

ENV DUSER cv_user
ENV DPASSWORD cv_user
ENV PATH="/usr/local/cuda/bin:/home/${DUSER}/.local/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64:${LD_LIBRARY_PATH}"

# Install apt requirements
RUN apt-get update -y \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y sudo wget python3 python3-pip git vim tmux \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update -y \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y ffmpeg libsm6 libxext6 \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash ${DUSER} \ 
    && echo "${DUSER}:${DPASSWORD}" | chpasswd
RUN adduser ${DUSER} sudo

WORKDIR /project
RUN chown ${DUSER}:${DUSER} /project

USER ${DUSER}

COPY --chown=${DUSER}:${DUSER} ./requirements.txt /project/requirements.txt

# Install pip requirements

RUN python3 -m pip install -U pip
RUN python3 -m pip install \
        --no-cache-dir \
        torch torchvision \
    && python3 -m pip install \
        --no-cache-dir \
        --ignore-installed \
        -r ./requirements.txt
